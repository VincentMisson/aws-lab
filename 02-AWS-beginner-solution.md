# AWS beginner

## Create your first Intranet VM in AWS using AWS portal
- Using EC2 services create your VM with the following characteristics :
![](images/beginner-1.png)
   - Image : Amazon Linux 2 AMI (HVM) - Kernel 5.10
   ![](images/beginner-2.png)
   - Type : t2.micro
   ![](images/beginner-3.png)
   - Network : vpc-0978b631b55caf241
   - Subnet : subnet-0af733ec1a336d01c (intranet-1a)
   - Auto-assign Public IP : Disable
   ![](images/beginner-4.png)
   - Tag Key : Name / Tag Value : *Name of your new VM*
   ![](images/beginner-5.png)
   - Select an existing security group : LabStandardSecurityGroup
   - When you launch the instance, you will be prompt to create a new key pair. You have to download the private key file (.pem or .ppk file). You will not be able to download the file again after it's created.
   ![](images/beginner-6.png)

- If you are connected on Michelin Network (on site or VPN), you must be able to connect to your VM using the private IP : 10.235.X.X
  - WSL : ssh -i vincent.pem ec2-user@10.235.64.117
  - Putty : 
    - Use PuTTYgen to convert the key file : Conversions -> Import key -> Select pem file with the key
    - Click : Save private key
    - Double click on the generated ppk file to load the key in Pageant
    - Open PuTTY, connect on your private IP wiht user : ec2-user

  ## ## Install Docker + nginx
- Install docker engine on your VM : 
  - Update your VM : [ec2-user ~]$ sudo yum update -y
  - Install Docker : [ec2-user ~]$ sudo yum install docker -y
  - Start the Docker Service : [ec2-user ~]$ sudo service docker start
  - Add the ec2-user to the docker group : [ec2-user ~]$ sudo usermod -a -G docker ec2-user
  - Deploy nginx container : docker run -d -p 80:80 --name nginx nginx

## Install Nextcloud using PostgreSQL DB PaaS as backend
- On your VM :
  - Deploy Network container on port 8080 : docker run -d -p 8080:80 --name nextcloud nextcloud
  - Confirgure Nextcloud to use your PostgreSQL DB as backend : 
    - User : postgres
    - DB name : postgres
    - DB endpoint : database-1.cli771wymhhi.eu-west-1.rds.amazonaws.com


## Expose your application over Internet
- Using EC2 service, create a loadbalancer to expose your application over Internet
    - Create your Target Group pointing your instance
      Target group :
        - Instances
        - Portocol : 80
        - VPC : vpc-0ac6f5d2bc06f9b55
        - Health checks : HTTP
        - Advanced health check settings : Success codes : 200,302
    - Register your instance as target in your Target Group
    - Create a new Application Load Balancer using your Target Group
      Type : Application Load Balancer
      Scheme : Internet-facing
      IP type : IPv4
      VPC : vpc-0ac6f5d2bc06f9b55
    - Using SG : LoadBalancerSecurityGroup
- Try to access your nginx using external URL
