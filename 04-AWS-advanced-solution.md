# AWS advanced

Let's do the same but in Infrastructure As Code

## Terraform installation

### Easy way 
- Start a CloudShell
- Install Terraform directly on the CloudShell : https://www.terraform.io/downloads

### Hard way (tested with WSL)
- Install AWS CLI
- Install Terraform


https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html


# AWS advanced

Let's do the same but in Infrastructure As Code

## Terraform installation

### Easy way 
- Start a CloudShell
- Install Terraform directly on the CloudShell : https://www.terraform.io/downloads

### Hard way (tested with WSL)
- Install AWS CLI
- Install Terraform

## Create your Terraform user
- Using IAM services create your Terraform user with the following characteristics : 
  - User name : Name of your Terraform user
  - Credential type : Access key - Programmatic access
  - Attach existing policies directly :  PowerUserAccess 
  - Note Access key ID +  Secret access key

## Configure your AWS env
- Using command : **aws configure** to share your access info with Terraform

## Create your first VM in AWS using Terraform
terraform/EC2/EC2-instance.tf

## Create your first PaaS DB in AWS using AWS portal 
terraform/RDS/PG-instance.tf

Install PostgreSQL Client : sudo amazon-linux-extras install postgresql13
Connect to the DB : psql -h <hostname or ip address> -p <port number of remote machine> -d <database name which you want to connect> -U <username of the database server>

Example : 
psql -h pg-terraform-vmi.cli771wymhhi.eu-west-1.rds.amazonaws.com -p 5432 -d postgres -U vincent
SELECT version();
