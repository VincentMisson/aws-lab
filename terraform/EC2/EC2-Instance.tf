terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14.9"
}
provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

variable "vm_name" {
  default = "MyFirstTerraform"
}

variable "ami_id" {
  description = "AMI ID"
  default     = "ami-0bf84c42e04519c85"
}

variable "subnet_id" {
  description = "Private Subnet ID"
  default     = "subnet-0be2fb4c80bb0ef6c"
}

variable "security_groups_id" {
  description = "Security Group ID"
  default     = ["sg-078477ca7752ba3cf"]
}

variable "key_name" {
  description = "SSH key name"
  default     = "vincent"
}

resource "aws_instance" "aws-instance" {
  ami                    = var.ami_id
  instance_type          = "t2.micro"
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.security_groups_id
  key_name               = var.key_name
  tags                   = { Name = var.vm_name }
}

output "private_ip" {
  description = "List of private IP addresses assigned to the instances"
  value       = aws_instance.aws-instance.*.private_ip
}