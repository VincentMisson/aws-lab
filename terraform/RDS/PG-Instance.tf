terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  required_version = ">= 0.14.9"
}
provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

variable "db_identifier" {
  default = "pg-terraform-vmi2"
}

variable "username" {
  default     = "vincent"
}

variable "password" {
  default     = "6UEK7MFNYHffAZ"
}

variable "subnet_ids" {
  default     = ["subnet-0be2fb4c80bb0ef6c", "subnet-08bd455c8c9b110cc"]
}

variable "security_groups_id" {
  description = "Security Group ID"
  default     = ["sg-00f1da230e382e0a7"]
}

resource "aws_db_subnet_group" "aws-db-subnet-group" {  
  name       = "db-subnet-group"  
  subnet_ids = var.subnet_ids
}

resource "aws_db_instance" "pg-instance" {  
  identifier             = var.db_identifier
  instance_class         = "db.t3.micro"  
  allocated_storage      = 5
  engine                 = "postgres"  
  engine_version         = "13.1"  
  username               = var.username  
  password               = var.password  
  db_subnet_group_name   = "db-subnet-group"
  vpc_security_group_ids = var.security_groups_id 
  publicly_accessible    = false  
  skip_final_snapshot    = true
}

output "endpoint" {
  value = aws_db_instance.pg-instance.endpoint 
}