# AWS beginner

## Connect to AWS
- Use this link to logon on AWS using your Michelin account : https://michelin-sso.awsapps.com/start#/
- Connect to **ShareLab** account using **AWSPowerUserAccess** role 
- Switch to Ireland (eu-west-1) region

![](images/image-1.jpeg)

## Create your first VM in AWS using AWS portal
- Using EC2 (Elastic Compute Cloud) service, create your VM with the following characteristics :
   - Image : Amazon Linux 2 AMI (HVM) - Kernel 5.10
   - Type : t2.micro
   - Network : vpc-0ac6f5d2bc06f9b55 (10.235.0.16.0/21)
   - Subnet : subnet-011a8007cbef22374 / subnet-08bd455c8c9b110cc / subnet-0be2fb4c80bb0ef6c
   - Auto-assign Public IP : Disable
   - Tag Key : Name / Tag Value : *Name of your new VM*
   - Select an existing security group : LabStandardSecurityGroup
   - When you launch the instance, you will be prompt to create a new key pair. You have to download the private key file (.pem file if you use WSL or .ppk if you use Putty). You will not be able to download the file again after it's created.
- If you are connected on Michelin Network (on site or VPN), you must be able to connect to your VM using the private IP : 10.235.X.X

![](images/image-6.png)

## Create your first PaaS DB in AWS using AWS portal 
- Using RDS service create a PostgreSQL database with the following characteristics : 
  - PostgreSQL 13.4-R1
  - Templates : Free tier
  - Single DB instance
  - DB instance identifier : Your Database Name
  - Provide Master password
  - DB instance class : Bustable classes - db.t3.micro
  - Connectivity : 
    - VPC : vpc-0ac6f5d2bc06f9b55
    - Public access : No
    - Security Group : DefaultPostgreSQL

DB creation will take few minutes to be up and running.
Note your database endpoint. 

## Install Docker + nginx
- On your VM : 
    - Install docker engine on your VM
    - Deploy nginx container on port 80
- Open your favorite browser : http://10.235.XX.XX (VM IP address)

## Install Nextcloud using PostgreSQL DB PaaS as backend
- On your VM : 
  - Deploy Network container on port 8080 : http://10.235.XX.XX:8080 (VM IP address)
  - Confirgure Nextcloud to use your PostgreSQL DB as backend

## Expose your application over Internet
- Using EC2 service, create a loadbalancer to expose your nginx over Internet
    - Create your Target Group pointing your instance
    - Register your instance as target in your Target Group
    - Create a new Application Load Balancer using your Target Group
    - Using SG : LoadBalancerSecurityGroup
- Try to access your nginx using external URL : xxxxxxxxxxxxxxx.eu-west-1.elb.amazonaws.com

## Remove all resources created using AWS portal
![](images/image-3.png)
    