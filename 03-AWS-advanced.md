# AWS advanced

Let's do the same but in Infrastructure As Code

![](images/image-2.jpeg)

## Terraform installation

### Easy way 
- Start a CloudShell
- Install Terraform directly on the CloudShell

### Hard way (tested with WSL)
- Install AWS CLI
- Install Terraform

## Create your Terraform user
- Using IAM services create your Terraform user with the following characteristics : 
  - User name : Name of your Terraform user
  - Credential type : Access key - Programmatic access
  - Attach existing policies directly :  PowerUserAccess 
  - Note Access key ID +  Secret access key

## Configure your AWS env
- Using command : **aws configure** to share your access info with Terraform

## Create your first VM in AWS using Terraform
- Create one Terraform file to create an EC2 instance with the following characteristics :
   - Image : Amazon Linux 2 AMI (HVM) - Kernel 5.10 (ami-0bf84c42e04519c85)
   - Type : t2.micro
   - Subnet : subnet-0be2fb4c80bb0ef6c
   - Tag Key : Name / Tag Value : *Name of your new VM*
   - Select an existing security group : LabStandardSecurityGroup (sg-078477ca7752ba3cf)
   - Use your existing key pair
- Get the private IP using the portal (bonus : using Terraform output to get the private IP)
- Connect to your VM using the private IP and your SSH key

## Create your first PaaS DB in AWS using AWS portal 
- Create one Terraform file to create a PostgreSQL database with the following characteristics :
  - PostgreSQL 13.1
  - DB instance class : db.t3.micro
  - Connectivity : 
    - Subnets : subnet-0be2fb4c80bb0ef6c / subnet-08bd455c8c9b110cc
    - Public access : No
    - Security Group : DefaultPostgreSQL (sg-00f1da230e382e0a7)
- Connect to your DB using PostgreSQL client

## Remove all resources created by Terraform
![](images/image-4.png)



